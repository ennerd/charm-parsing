<?php
namespace Charm\Parsing\Error;

/**
 * Exception thrown when lexing fails; for example if a token is not
 * recognized.
 */
class LexingError extends GrammarError {
}
