<?php
namespace Charm\Parsing\Error;

use Charm\Parsing\Util\StringTools;

/**
 * Error thrown when a regular expression contains an error
 */
class RegexError extends \Exception {
}
