<?php
namespace Charm\Parsing\Error;

/**
 * Error thrown when parsing a source code fails because the provided
 * source disagrees with the grammar.
 */
class SyntaxError extends Exception {
}
