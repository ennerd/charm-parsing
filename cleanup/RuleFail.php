<?php
namespace Charm\Parsing;

/**
 * A return value indicating that the entire sequence should fail.
 */
class SequenceFail implements SequenceInstruction {
}
