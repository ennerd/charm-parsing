<?php
namespace Charm\Parsing;

/**
 * The sequence should finish successfully without returning a token
 */
class SequenceIgnore implements SequenceInstruction {
}
