<?php
namespace Charm\Parsing;

/**
 * The sequence will return false and be treated as a missed parse.
 */
class SequenceFail implements SequenceInstruction {
}
