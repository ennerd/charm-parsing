<?php
namespace Charm\Parsing;

/**
 * The sequence should continue parsing the next token.
 */
class SequenceResume implements SequenceInstruction {
}
